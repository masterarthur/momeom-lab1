disp('Task 5');

N=3; % Номер в журнале
M=3; % 

A=(sin(N))^2; % Синус числа N в квадрате
B=log(N+2); % Натуральный логорифм из N + 2
C=exp(N/(N^2)); % e в степени N поделенное на N  квадрате
D=A+B; % Сумма А и Б

expression = (sqrt(A^(-2))+B^(1.7))/(D+C+A); % Отношение между суммой квдратного корня из  А минус второй степени и B в стеени 1.7 и суммы D, С и A

format short e;
disp('Result of the expression where product sings were changed to plus');
disp(expression);

for N = N:N+5
    disp('N is equal to ');
    disp(N);
    A=(sin(N)*cos(N))^2;
    B=log(N+2)/N;
    C=exp(N/(N^2))^(-2);
    D=A+B;

    expression = (A*B*C)/(A^2+D);
    disp('Expression is equal to ');
    disp(expression)
end
 

