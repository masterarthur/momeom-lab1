disp("Task 3");

firstName="Artur";
lastName="Khalatian";

a=strlength(firstName);
b=strlength(lastName);

z=a+b*1i;
zConjugate=z';

zConjugateInSquare = zConjugate^2;

product = z*zConjugate;

trigionomExpression = sin(z)+cos(zConjugate);


disp("Complex number z=a+b*i:");
disp(z);
disp("Complex conjugate number z:");
disp(zConjugate);

disp("Complex conjugate number z in square:");
disp(zConjugateInSquare);

disp("Product of complex number z and complex conjugate number z:");
disp(product);

disp("Result of trigonometric expression sin(a+b*i)+cos(a-b*i):");
disp(trigionomExpression);