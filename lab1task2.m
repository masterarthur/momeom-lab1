disp('Second task');
N=3
V=[19 77 45 11 67];
v=-5;
exp1=V+v;
exp2=exp1.\V;
exp3=exp2/v;
exp4=V.^N;
format short;
disp('Result of expression 1 in short format');
disp(exp1);
disp('Result of expression 2 in short format');
disp(exp2);
disp('Result of expression 3 in short format');
disp(exp3);
disp('Result of expression 4 in short format');
disp(exp4);

format short e;
disp('Result of expression 1 in "short e" format');
disp(exp1);
disp('Result of expression 2 in "short e" format');
disp(exp2);
disp('Result of expression 3 in "short e" format');
disp(exp3);
disp('Result of expression 4 in "short e" format');
disp(exp4);

format long;
disp('Result of expression 1 in long format');
disp(exp1);
disp('Result of expression 2 in long format');
disp(exp2);
disp('Result of expression 3 in long format');
disp(exp3);
disp('Result of expression 4 in long format');
disp(exp4);

format long e;
disp('Result of expression 1 in "long e" format');
disp(exp1);
disp('Result of expression 2 in "long e" format');
disp(exp2);
disp('Result of expression 3 in "long e" format');
disp(exp3);
disp('Result of expression 4 in "long e" format');
disp(exp4);