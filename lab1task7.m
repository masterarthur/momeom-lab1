%Седьмое задание
disp("Task 7");


N=3; %номер по списку (03)
C=0; %старшая цифра
M=3; %младшая цифра

A_matrix=[1 3 N; 2 C M; N 9 8];
B_matrix=[6 C N; M C 1; 3 5 2];
C_matrix=[C M N; C C N; M N C];
disp(A_matrix);
disp(B_matrix);
disp(C_matrix);

A_matrix_T=A_matrix';
disp(A_matrix_T);

B_matrix_T=B_matrix';
disp(B_matrix_T);

matrix_expr=(A_matrix+C_matrix)*B_matrix^3*(A_matrix-C_matrix)';
disp(matrix_expr);

C1_matrix=complex(A_matrix,B_matrix);
disp(C1_matrix);

C1_matrix_sort=C1_matrix';
disp(C1_matrix_sort);

A_matrix_square=A_matrix^2;
disp(A_matrix_square);

A_matrix_proizv=A_matrix*A_matrix;
disp(A_matrix_proizv);

AB_expression1=A_matrix(1,:)* B_matrix;
AB_expression2=B_matrix*A_matrix(:,3);

disp(AB_expression1);
disp(AB_expression2);
K_matrix=[A_matrix, B_matrix; B_matrix_T, A_matrix_T];
disp(K_matrix);
K_matrix(:,2)=[];
disp(K_matrix);