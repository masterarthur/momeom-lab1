disp("Task 6");

N=3; %номер по списку (03)
C=0; %старшая цифра
M=3; %младшая цифра

x=[0.2 0.3 0.5 0.8 M C N];
disp(x);

y=sin(x).^2/(1+cos(x))+exp(-x).*log(x);
disp(y);

v=[x y(2:7)];

v1=v+2*N;
w=v-v1;
w1=v-v1;
w2=v1-v;
w3=v./v1;
w4=v1.*v;
w5=v1.^v;

W_abs=sort(abs(w));
disp(W_abs);

W_1=sort(w);
disp(W_1);

W_2=sort(-w);
disp(W_2);

ww=[w1(3); w1(5); w2(3); w2(5); w3(3); w3(5); w4(3); w4(5); w5(3); w5(5)];
disp(ww);

min_ww=min(ww);
max_ww=max(ww);
prod_ww=prod(ww);

disp(min_ww);
disp(max_ww);
disp(prod_ww);