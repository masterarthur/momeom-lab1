disp('Task 4');

N=3; % Номер в журнале
M=3; % 

A=(sin(N))^2; % Синус числа N в квадрате
B=log(N+2); % Натуральный логорифм из N + 2
C=exp(N/(N^2)); % e в степени N поделенное на N  квадрате
D=A+B; % Сумма А и Б

expression = (sqrt(A^(-2))*B^(1.7))/(D+C*A); % Отношение между произвидением квдратного корня из  А минус второй степени и B в стеени 1.7 и суммы D с произвидением С и A

format short e;
disp('A in "short e" format');
disp(A);
disp('B in "short e" format');
disp(B);
disp('C in "short e" format');
disp(C);
disp('D in "short e" format');
disp(D);

disp('Result of the expression in "short e" format');
disp(expression);

format long;
disp('A in long format');
disp(A);
disp('B in long format');
disp(B);
disp('C in long format');
disp(C);
disp('D in long format');
disp(D);

disp('Result of the expression in long format');
disp(expression);

format long e;
disp('A in "long e" format');
disp(A);
disp('B in "long e" format');
disp(B);
disp('C in "long e" format');
disp(C);
disp('D in "long e" format');
disp(D);

disp('Result of the expression in "long e" format');
disp(expression);